package com.nbtf.pic;
import com.nbtf.pic.SecondLevelDir;
import java.util.ArrayList;


/**
 * Created by leo on 14/12/19.
 */
public class FirstLevelDir {
    private String topic;
    private String dirUrl;
    private ArrayList<SecondLevelDir> secondLevelDirs = new ArrayList<SecondLevelDir>();

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getDirUrl() {
        return dirUrl;
    }

    public void setDirUrl(String dirUrl) {
        this.dirUrl = dirUrl;
    }

    public ArrayList<SecondLevelDir> getSecondLevelDirs() {
        return secondLevelDirs;
    }

    public void setSecondLevelDirs(ArrayList<SecondLevelDir> secondLevelDirs) {
        this.secondLevelDirs = secondLevelDirs;
    }

    @Override
    public String toString() {
        return "FirstLevelDir{" +
                "topic='" + topic + '\'' +
                ", dirUrl='" + dirUrl + '\'' +
                ", secondLevelDirs=" + secondLevelDirs +
                '}'+'\n';
    }
}
