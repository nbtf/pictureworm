package com.nbtf.pic;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import java.security.MessageDigest;
import org.apache.commons.codec.binary.Hex;
/**
 * Created by leo on 14/12/18.
 */
public class Worm extends Thread {
    private ArrayList<String> keywords;
    private String prefix;
    private static final Log logger = LogFactory.getLog(Worm.class);
    private ArrayList<FirstLevelDir> firstLevelDirs = null;
    private MessageDigest md5 = null;
    public Worm(ArrayList<String> keywords,String prefix ){
        this.keywords = keywords;
        this.prefix = prefix;
        try {
            md5 = MessageDigest.getInstance("md5");
        } catch (NoSuchAlgorithmException e) {
            logger.error("GET MD5 MESSAGEDIGEST INSTANCE FAILED");
            e.printStackTrace();
        }
    }
    public Worm(ArrayList<FirstLevelDir> flds){
        this.firstLevelDirs = flds;
        try {
            md5 = MessageDigest.getInstance("md5");
        } catch (NoSuchAlgorithmException e) {
            logger.error("GET MD5 MESSAGEDIGEST INSTANCE FAILED");
            e.printStackTrace();
        }
    }
    public String getCommonImageUrl(Elements el){
        if(el == null){
            return null;
        }
        if(!el.isEmpty()) {
            return el.first().attr("src");
        }
        return null;
    }

    public String getStarImageUrl(Elements el){
        if(el == null){
            return null;
        }
        if (!el.isEmpty()){
            return el.first().getElementsByTag("img").attr("src");
        }

        return null;
    }
    public String getImage(String imgurl,String name,String dir){
        if (imgurl == null){
            return null;
        }
        int dotindex = imgurl.lastIndexOf(".");
        String pictype = imgurl.substring(dotindex+1);
        String picname = name+'.'+pictype;
        try {

            URL url = new URL(imgurl);
            BufferedImage image = null;
            image = ImageIO.read(url);

            ImageIO.write(image,pictype,new File(dir+picname));
            return picname;
        } catch (Exception e) {
            logger.error("GET IMAGE FAILED: "+"["+name+"]"+"["+imgurl+"]"+e.toString());
            System.out.println("GET IMAGE FAILED: " + "[" + name + "]" + "[" + imgurl + "]" + e.toString());
        }
        return picname;
    }

    public void parserFirstLevelDir(){
        for(FirstLevelDir f:firstLevelDirs) {
            //System.out.println(firstLevelDir.getSecondLevelDirs().toString());
            String fTopic = f.getTopic();
            for (SecondLevelDir s : f.getSecondLevelDirs()) {
                try {
                    Document firstSecondLevelDirDoc = Jsoup.connect(s.getDirUrl()).get();
                    ArrayList<String> secondLevelUrls = new ArrayList<String>();
                    secondLevelUrls.add(s.getDirUrl());
                    String sTopic = s.getTopic();
                    String dir = "imgs/" + fTopic + "/" + sTopic + "/";
                    File fileDir = new File(dir);
                    fileDir.mkdirs();
                    String prefix = s.getDirUrl().substring(0, s.getDirUrl().indexOf("com") + 3);
                    Elements paginationElements = firstSecondLevelDirDoc.getElementsByClass("pagination");
                    String topicSort = fTopic + sTopic;
                    if (paginationElements != null && !paginationElements.isEmpty()) {
                        Elements pageElements = paginationElements.first().getElementsByTag("a");
                        if (!pageElements.isEmpty()) {
                            Element tailpageElement = pageElements.first().lastElementSibling();
                            String lastUrl = tailpageElement.attr("href");
                            System.out.println(lastUrl);
                            int equalIndex = lastUrl.lastIndexOf("=");
                            int lastPage = 2;
                            try {
                                lastPage = Integer.parseInt(lastUrl.substring(equalIndex + 1));
                            } catch (Exception e) {
                                logger.error("GET THE LAST PAGE INDEX FAILED " + e.toString());
                            }
                            //System.out.println(equalIndex);
                            String baseUrl = lastUrl.substring(0, equalIndex + 1);
                            if (lastPage > 1) {
                                for (int i = 2; i <= lastPage; i++) {
                                    secondLevelUrls.add(prefix + baseUrl + i);
                                }
                            }
                            //System.out.println(baseUrl);
                            //System.out.println(lastPage);
                            System.out.println(secondLevelUrls);
                            //System.out.println(pageElements.toString());
                            //System.out.println(tailpageElement.toString());
                        }
                    }
                    for (String secondlevelurl : secondLevelUrls) {
                        Document secondLevelDirDoc = Jsoup.connect(secondlevelurl).get();
                        Elements thirdLevelEls = secondLevelDirDoc.getElementsByClass("ba_href");
                        for (Element e : thirdLevelEls) {
                            Elements imgElemets = e.getElementsByTag("img");

                            ThirdLevelDir thirdLevelDir = new ThirdLevelDir();
                            String thirdLevelDirUrl = e.attr("href");
                            thirdLevelDir.setThirdLevelDirUrl(thirdLevelDirUrl);

                            Elements baNameElements = e.getElementsByClass("ba_name");
                            String topic = null;
                            if (baNameElements != null && !baNameElements.isEmpty()) {
                                topic = baNameElements.first().text();
                                topic = topic.substring(0, topic.length() - 1);
                                thirdLevelDir.setThirdLevelDirTopic(topic);
                            } else {
                                continue;
                            }
                            Elements baNumElements = e.getElementsByClass("ba_p_num");
                            int memNum = 0;
                            if (baNumElements != null && !baNumElements.isEmpty()) {
                                try {
                                    memNum = Integer.parseInt(baNumElements.first().text());
                                    thirdLevelDir.setThirdLevelDirMnum(memNum);
                                } catch (Exception e1) {
                                    logger.error("GET THE MEMBER NUMBER FAILED [" + topic + "]");
                                }

                            }
                            String baDesc = null;
                            Elements baDescElements = e.getElementsByClass("ba_desc");
                            if (baDescElements != null && !baDescElements.isEmpty()) {
                                baDesc = baDescElements.first().text();
                                thirdLevelDir.setThirdLevelDirDesc(baDesc);
                            }

                            String md5Name = Hex.encodeHexString(md5.digest((topicSort + topic).getBytes()));

                            String imgUrl = null;
                            if (imgElemets != null && !imgElemets.isEmpty()) {
                                imgUrl = imgElemets.first().attr("src");
                                if (imgUrl != null) {
                                    thirdLevelDir.setThirdLevelDirPicUrl(imgUrl);
                                } else {
                                    logger.error("GET IMAGE URL FAILRD FRO [" + topic + "]");
                                }
                            }
                            s.getThirdLevelDirs().add(thirdLevelDir);
                            getImage(imgUrl, topic, dir);
                            String md5pic = getImage(imgUrl, md5Name, "imgs/");
                            logger.info("[THRIDLEVELDIRINFO]\t" + topic + "\t" + md5Name + "\t" + fTopic + "\t" + sTopic + "\t" + memNum + "\t" + md5pic);
                        }
                        //System.out.println(thirdLevelEls.toString());
                        //System.out.println(thirdLevelEls.size());
                    }
                    System.out.println(s.getThirdLevelDirs().size());
                    //System.out.println(s.getThirdLevelDirs().toString());
                    //break;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            System.out.println(f.toString());
        }

    }
    public void run(){
        parserFirstLevelDir();
//        parserZeroLevelUrl();
        /*
        for(String keyword:keywords){

            String pageurl = prefix+keyword;

            try {
                System.out.println(pageurl);
                Document doc = Jsoup.connect(pageurl).get();
                if(doc == null){
                    logger.error("NO WEBPAGE GET FOR ["+ keyword+"]");
                    return;
                }
                Elements card_head_img_e = doc.getElementsByClass("card_head_img");
                Elements star_pic_box_e = doc.getElementsByClass("star_picbox");
                String imgurl_common = getCommonImageUrl(card_head_img_e);
                String imgurl_star = getStarImageUrl(star_pic_box_e);
                if( imgurl_common != null) {
                    getImage(imgurl_common, keyword);
                }else if(imgurl_star != null) {
                    getImage(imgurl_star, keyword);
                }else{
                    logger.error("NO IMGURL GET FOR ["+ keyword+"]");
                    logger.error("WEBPAGE \n["+ doc.toString()+"]");
                }
            } catch (IOException e) {
                logger.error(e.toString());
            }
        }*/
    }


}
