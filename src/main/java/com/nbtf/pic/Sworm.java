package com.nbtf.pic;
import com.nbtf.pic.FirstLevelDir;
import com.nbtf.pic.Worm;
import java.io.*;
import java.util.ArrayList;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.nodes.Document;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


/**
 * Created by leo on 14/12/18.
 */
public class Sworm {

    private static final Log logger = LogFactory.getLog(Sworm.class);
    private static final String prefix =  "http://tieba.baidu.com/f?ie=utf-8&fr=search&kw=";
    private static final String topicprefix = "http://tieba.baidu.com";
    private static int WORMNUM = 4;
    private static ArrayList<ArrayList<String>>  topics = new ArrayList<ArrayList<String>>(WORMNUM);
    private static ArrayList<Worm> worms = new ArrayList<Worm>();
    private static ArrayList<FirstLevelDir> firstLevelDirs = new ArrayList<FirstLevelDir>();

    public  static void main(final String[] args){
        File dir = new File("imgs");
        dir.mkdir();
        System.out.println("USAGE:\n" +
                "java -jar jarfile [topicfile e.g. topic.txt]");
        if(args.length != 1 ){
            System.out.println("REFER TO THE USAGE");
        }
        String topicFile = "topics.txt";//args[0];
        initTopicList();
        //readTopics(topicFile);
        parserZeroLevelUrl();
        sendWorms();
    }
    private static void initTopicList(){
        for(int i = 0;i<WORMNUM;i++){
            ArrayList<String> a = new ArrayList<String>();
            topics.add(a);
        }
    }

    private static void readTopics(String topicfile){
        try {
            FileReader fileReader = new FileReader(topicfile);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;
            int i = 0;
            int arindex = 0;
            while( (line = bufferedReader.readLine()) != null){

                arindex = i++ % WORMNUM;
                System.out.println(arindex);
                topics.get(arindex).add(line.trim());
                System.out.println(line.toString());
            }

        } catch (FileNotFoundException e) {
            logger.error(e.toString());
        } catch (IOException e) {
            logger.error(e.toString());
        }
    }
    public static void parserZeroLevelUrl(){
        String zeroLevelUrl="http://tieba.baidu.com/f/index/forumclass";
        try {
            Document zeroLevelDoc = Jsoup.connect(zeroLevelUrl).get();
            if(zeroLevelDoc == null){
                logger.error("NO WEBPAGE GET FOR ZREOLEVEL URL"+"["+zeroLevelUrl+"]");
                System.exit(1);
            }
            Elements firstLevelItems = zeroLevelDoc.getElementsByClass("class-item");
            for(Element el:firstLevelItems){
                FirstLevelDir firstLevelDir = new FirstLevelDir();
                Elements topicEls = el.getElementsByTag("a");
                //System.out.println(topicEls.toString());

                if(!topicEls.isEmpty()){
                    Element firstLevelDirEl = topicEls.first();
                    String firstLevelDirTopic = firstLevelDirEl.text();
                    String firstLevelDirUrl = firstLevelDirEl.attr("href");
                    firstLevelDir.setTopic(firstLevelDirEl.text());
                    firstLevelDir.setDirUrl(firstLevelDirUrl);
//                    System.out.println("------------------------------------------------------------");
//                    System.out.println(firstLevelDirTopic +" url "+firstLevelDirUrl);
//                    System.out.println(firstLevelDirUrl);
                    int head = 1;
                    int tail = topicEls.size();
                    for(;head<tail;head++){
                        Element secondLevelEl = topicEls.get(head);
                        String secondLevelTopic = secondLevelEl.text();
                        String secondLevelUrl =  topicprefix+secondLevelEl.attr("href");
                        SecondLevelDir secondLevelDir = new SecondLevelDir();
                        secondLevelDir.setTopic(secondLevelTopic);
                        secondLevelDir.setDirUrl(secondLevelUrl);
                        firstLevelDir.getSecondLevelDirs().add(secondLevelDir);
                        //System.out.println(secondLevelDir.getTopic()+" url " + secondLevelDir.getDirUrl());
                    }
                }
                firstLevelDirs.add(firstLevelDir);
                //System.out.println("------------------------------------------------------------");
                //Elements secondDirEls = el.getElementsByTag("li");
                //System.out.println(topicEls.toString());
                //System.out.println(secondDirEls.toString());
                //break;
            }
//            System.out.println(firstLevelDirs.toString());
            System.out.println(firstLevelDirs.get(12).toString());
            //System.out.println(firstLevelDirs.get(14).toString());
            //System.out.println(firstLevelDirs.get(14).toString());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void sendWorms(){
        ArrayList<String> a = new ArrayList<String>();
        //for(FirstLevelDir f:firstLevelDirs){
            Worm worm= new Worm(firstLevelDirs);
            worms.add(worm);
            worm.start();

       // }
//        System.out.println(firstLevelDirs.get(0));
//        Worm worm= new Worm(firstLevelDirs.get(0));
//
//        worm.start();
        /*
        for(ArrayList<String> a:topics) {
            Worm worm= new Worm(a,prefix);
            worms.add(worm);
            worm.start();
        }*/
    }

}
